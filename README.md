# URL Generator for Australian Yellow Pages

Simple url generator for [yellowpages.com.au](https://www.yellowpages.com.au/).

## Getting Started

### Prerequisites

* [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [python 3](https://docs.python.org/3/using/index.html)

### Installing

Clone project:

```
git clone git@bitbucket.org:nsalikov/ypau.git
```

## Usage

You need to prepare file (or files) with list of locations, one location per line. Locations may be any that Yellow Pages supports (states, cities, streets, postal codes and so on).

Then simply run the script with this file and search query as arguments:

```
python ypau.py locations.txt plumbers
```

You can specify multiple queries as well:

```
python ypau.py locations.txt plumbers carpenter
```

If search query contains spaces, enclose it in double quotes:

```
python ypau.py locations.txt "gas station"
```

By default script will print URL on screen. You can specify optional parametr --output to save it into file:

```
python ypau.py locations.txt --output url.txt  plumbers carpenter "gas station"
```