# -*- coding: utf-8 -*-

import click
import urllib.parse
import sys

#######################################################
# Configuration
#######################################################

URL = 'https://www.yellowpages.com.au/search/listings?clue={search}&locationClue={location}&lat=&lon=&selectedViewMode=list'

#######################################################
# Command
#######################################################

@click.command()
@click.option('output', '--output', type=click.File('w'), default='-')
@click.argument('locations', nargs=1, type=click.File('r'))
@click.argument('searches', nargs=-1, required=True)
def generate_url(searches, locations, output):

    for s in searches:
        for loc in locations:
            url = URL.format(search=urllib.parse.quote(s.strip()), location=urllib.parse.quote(loc.strip()))
            output.write(url + '\n')

#######################################################
# Main
#######################################################

if __name__ == '__main__':
    generate_url()
